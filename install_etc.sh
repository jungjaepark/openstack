#! /bin/bash
set -e 

install_dashboard() {
sudo yum -y install openstack-dashboard
sudo cp local_settings.dashboard /etc/openstack-dashboard/local_settings
echo "WSGIApplicationGroup %{GLOBAL}" | sudo tee -a /etc/httpd/conf.d/openstack-dashboard.conf
sudo systemctl restart httpd.service memcached.service
}

install_dashboard
