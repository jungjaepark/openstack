#! /bin/bash
if [ $# != 3 ]; then
	echo "$0 <flavor> <image> <name>"
fi
FLAVOR=$1
IMAGE=$2
NAME=$3
. demo-openrc
openstack server create --flavor $FLAVOR \
	--image $IMAGE \
	--nic net-id=$(openstack network list | awk '/selfservice/{print $2}') --security-group default \
	--key-name mykey $NAME
openstack server list
