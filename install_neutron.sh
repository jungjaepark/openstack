#! /bin/bash
set -e

IPADDR=192.168.0.156
#IFACE=enp0s31f6
#IFACE=enp4s0
IFACE=enp4s0

neutron_controller() {
mysql -u root -pDB_PASS < neutron.db
. admin-openrc
openstack user create --domain default --password NEUTRON_PASS neutron
openstack role add --project service --user neutron admin
openstack service create --name neutron \
  --description "OpenStack Networking" network
openstack endpoint create --region RegionOne \
  network public http://controller:9696
openstack endpoint create --region RegionOne \
  network internal http://controller:9696
openstack endpoint create --region RegionOne \
  network admin http://controller:9696
sudo yum -y install openstack-neutron openstack-neutron-ml2 \
  openstack-neutron-linuxbridge ebtables
sudo crudini --merge /etc/neutron/neutron.conf < neutron.conf.crudini
sudo crudini --merge /etc/neutron/plugins/ml2/ml2_conf.ini < ml2_conf.ini.crudini
sed -e "s/PROVIDER_INTERFACE_NAME/$IFACE/g" \
	-e "s/OVERLAY_INTERFACE_IP_ADDRESS/$IPADDR/g" \
	linuxbridge_agent.ini.crudini |
	sudo crudini --merge /etc/neutron/plugins/ml2/linuxbridge_agent.ini
sudo modprobe br_netfilter
sudo sysctl -w net.bridge.bridge-nf-call-iptables=1
sudo sysctl -w net.bridge.bridge-nf-call-ip6tables=1
sudo crudini --merge /etc/neutron/l3_agent.ini < l3_agent.ini.crudini
sudo crudini --merge /etc/neutron/dhcp_agent.ini < dhcp_agent.ini.crudini
sudo crudini --merge /etc/neutron/metadata_agent.ini < metadata_agent.ini.crudini
sudo crudini --merge /etc/nova/nova.conf < nova.conf.crudini.neutron
sudo ln -s /etc/neutron/plugins/ml2/ml2_conf.ini /etc/neutron/plugin.ini
sudo su -s /bin/sh -c "neutron-db-manage --config-file /etc/neutron/neutron.conf \
  --config-file /etc/neutron/plugins/ml2/ml2_conf.ini upgrade head" neutron
sudo systemctl restart openstack-nova-api.service
sudo systemctl enable neutron-server.service \
  neutron-linuxbridge-agent.service neutron-dhcp-agent.service \
  neutron-metadata-agent.service
sudo systemctl start neutron-server.service \
  neutron-linuxbridge-agent.service neutron-dhcp-agent.service \
  neutron-metadata-agent.service
sudo systemctl enable neutron-l3-agent.service
sudo systemctl start neutron-l3-agent.service
}

neutron_compute() {
sudo yum -y install openstack-neutron-linuxbridge ebtables ipset
sudo crudini --merge /etc/neutron/neutron.conf < neutron.conf.crudini.compute
sed -e "s/PROVIDER_INTERFACE_NAME/$IFACE/g" \
        -e "s/OVERLAY_INTERFACE_IP_ADDRESS/$IPADDR/g" \
        linuxbridge_agent.ini.crudini.compute |
	sudo crudini --merge /etc/neutron/plugins/ml2/linuxbridge_agent.ini
sudo modprobe br_netfilter
sudo sysctl -w net.bridge.bridge-nf-call-iptables=1
sudo sysctl -w net.bridge.bridge-nf-call-ip6tables=1
sudo crudini --merge /etc/nova/nova.conf < nova.conf.crudini.neutron.compute
sudo systemctl restart openstack-nova-compute.service
sudo systemctl enable neutron-linuxbridge-agent.service
sudo systemctl start neutron-linuxbridge-agent.service
}

if [ $# -ne 1 ]; then
  exit 1
fi

if [ $1 = "controller" ]; then
  neutron_controller
elif [ $1 = "compute" ]; then
  neutron_compute
fi
