#! /bin/bash
set -e

IPADDR=192.168.0.156
sudo yum -y install mariadb mariadb-server python2-PyMySQL
sed -e s/10.0.0.11/$IPADDR/g openstack.cnf | sudo tee /etc/my.cnf.d/openstack.cnf
sudo systemctl enable mariadb.service
sudo systemctl start mariadb.service
sudo mysql_secure_installation
