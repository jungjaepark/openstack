#! /bin/bash
set -e 
#plz change IP addres.
IPADDR=192.168.0.156

nova_controller() {
mysql -u root -pDB_PASS < nova.db
. admin-openrc
openstack user create --domain default --password NOVA_PASS nova
openstack role add --project service --user nova admin
openstack service create --name nova \
  --description "OpenStack Compute" compute
openstack endpoint create --region RegionOne \
  compute public http://controller:8774/v2.1
openstack endpoint create --region RegionOne \
  compute internal http://controller:8774/v2.1
openstack endpoint create --region RegionOne \
  compute admin http://controller:8774/v2.1
openstack user create --domain default --password PLACEMENT_PASS placement
openstack role add --project service --user placement admin
openstack service create --name placement \
  --description "Placement API" placement
openstack endpoint create --region RegionOne \
  placement public http://controller:8778
openstack endpoint create --region RegionOne \
  placement internal http://controller:8778
openstack endpoint create --region RegionOne \
  placement admin http://controller:8778
sudo yum -y install openstack-nova-api openstack-nova-conductor \
  openstack-nova-console openstack-nova-novncproxy \
  openstack-nova-scheduler openstack-nova-placement-api
sed -e "s/10.0.0.11/$IPADDR/g" nova.conf.crudini | 
	sudo crudini --merge /etc/nova/nova.conf
cat 00-nova-placement-api.conf | sudo tee -a /etc/httpd/conf.d/00-nova-placement-api.conf
sudo systemctl restart httpd
sudo su -s /bin/sh -c "nova-manage api_db sync" nova
sudo su -s /bin/sh -c "nova-manage cell_v2 map_cell0" nova
sudo su -s /bin/sh -c "nova-manage cell_v2 create_cell --name=cell1 --verbose" nova
sudo su -s /bin/sh -c "nova-manage db sync" nova
sudo systemctl enable openstack-nova-api.service \
  openstack-nova-consoleauth openstack-nova-scheduler.service \
  openstack-nova-conductor.service openstack-nova-novncproxy.service
sudo systemctl start openstack-nova-api.service \
  openstack-nova-consoleauth openstack-nova-scheduler.service \
  openstack-nova-conductor.service openstack-nova-novncproxy.service
}

nova_compute() {
sudo yum -y install openstack-nova-compute
sed -e "s/MANAGEMENT_INTERFACE_IP_ADDRESS/$IPADDR/g" nova.conf.crudini.compute |
	sudo crudini --merge /etc/nova/nova.conf
sudo systemctl enable libvirtd.service openstack-nova-compute.service
sudo systemctl start libvirtd.service openstack-nova-compute.service
}

if [ $# -ne 1 ]; then
  exit
fi

if [ $1 = "controller" ]; then
  nova_controller
elif [ $1 = "compute" ]; then
  nova_compute
fi
