#! /bin/bash
set -e

mysql -u root -pDB_PASS < glance.db
. admin-openrc
openstack user create --domain default --password GLANCE_PASS glance
openstack role add --project service --user glance admin
openstack service create --name glance \
  --description "OpenStack Image" image
openstack endpoint create --region RegionOne \
  image public http://controller:9292
openstack endpoint create --region RegionOne \
  image internal http://controller:9292
openstack endpoint create --region RegionOne \
  image admin http://controller:9292
sudo yum -y install openstack-glance
sudo crudini --merge /etc/glance/glance-api.conf < glance-api.conf.crudini
sudo crudini --merge /etc/glance/glance-registry.conf < glance-registry.conf.crudini
sudo su -s /bin/sh -c "glance-manage db_sync" glance
sudo systemctl enable openstack-glance-api.service \
  openstack-glance-registry.service
sudo systemctl start openstack-glance-api.service \
  openstack-glance-registry.service
