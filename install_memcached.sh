#! /bin/bash
set -e

sudo yum -y install memcached python-memcached
sudo cp /etc/sysconfig/memcached /etc/sysconfig/memcached.bak
sudo cp memcached /etc/sysconfig/memcached
sudo systemctl enable memcached.service
sudo systemctl start memcached.service
