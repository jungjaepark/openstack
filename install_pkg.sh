#! /bin/bash
set -e

sudo yum -y install centos-release-openstack-rocky
sudo yum -y upgrade
sudo yum -y install python-openstackclient
sudo yum -y install openstack-selinux

sudo yum -y install crudini
sudo systemctl stop firewalld
sudo systemctl disable firewalld
