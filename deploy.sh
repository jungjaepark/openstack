#! /bin/bash

START_IP_ADDRESS=192.168.0.231
END_IP_ADDRESS=192.168.0.250
PROVIDER_NETWORK_GATEWAY=192.168.0.1
PROVIDER_NETWORK_CIDR="192.168.0.0/24"
DNS_RESOLVER=8.8.8.8
SELFSERVICE_NETWORK_GATEWAY=10.0.0.1
SELFSERVICE_NETWORK_CIDR="10.0.0.0/24"

if false; then
. admin-openrc
openstack network create  --share --external \
  --provider-physical-network provider \
  --provider-network-type flat provider
openstack subnet create --network provider \
  --allocation-pool start=$START_IP_ADDRESS,end=$END_IP_ADDRESS \
  --dns-nameserver $DNS_RESOLVER --gateway $PROVIDER_NETWORK_GATEWAY \
  --subnet-range $PROVIDER_NETWORK_CIDR provider
. demo-openrc
openstack network create selfservice
openstack subnet create --network selfservice \
  --dns-nameserver $DNS_RESOLVER --gateway $SELFSERVICE_NETWORK_GATEWAY \
  --subnet-range $SELFSERVICE_NETWORK_CIDR selfservice
openstack router create router
openstack router add subnet router selfservice
openstack router set router --external-gateway provider
. admin-openrc
openstack flavor create --id 0 --vcpus 1 --ram 64 --disk 1 m1.nano
. demo-openrc
ssh-keygen -q -N ""
openstack keypair create --public-key ~/.ssh/id_rsa.pub mykey
openstack security group rule create --proto icmp default
openstack security group rule create --proto tcp --dst-port 22 default
fi
. demo-openrc
openstack server create --flavor m1.nano --image cirros \
  --nic net-id=$(openstack network list | awk '/selfservice/{print $2}')  --security-group default \
  --key-name mykey selfservice-instance
