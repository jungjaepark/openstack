#! /bin/bash
set -e

IPADDR=192.168.0.156
sudo yum -y install etcd
sudo cp /etc/etcd/etcd.conf /etc/etcd/etcd.conf.bak
sed -e s/10.0.0.11/$IPADDR/g etcd.conf | sudo tee /etc/etcd/etcd.conf
sudo systemctl enable etcd
sudo systemctl start etcd
