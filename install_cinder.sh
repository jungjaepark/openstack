#! /bin/bash
set -e
IP=192.168.0.156
CINDER_DEV=/dev/sdb3

install_controller() {
cat << EOF | mysql -u root -pDB_PASS
CREATE DATABASE cinder;
GRANT ALL PRIVILEGES ON cinder.* TO 'cinder'@'localhost' IDENTIFIED BY 'CINDER_DBPASS';
GRANT ALL PRIVILEGES ON cinder.* TO 'cinder'@'%' IDENTIFIED BY 'CINDER_DBPASS';
EOF
. ./admin-openrc
openstack user create --domain default --password CINDER_PASS cinder
openstack role add --project service --user cinder admin
openstack service create --name cinderv2 \
  --description "OpenStack Block Storage" volumev2
openstack service create --name cinderv3 \
  --description "OpenStack Block Storage" volumev3
openstack endpoint create --region RegionOne \
  volumev2 public http://controller:8776/v2/%\(project_id\)s
openstack endpoint create --region RegionOne \
  volumev2 internal http://controller:8776/v2/%\(project_id\)s
openstack endpoint create --region RegionOne \
  volumev2 admin http://controller:8776/v2/%\(project_id\)s
openstack endpoint create --region RegionOne \
  volumev3 public http://controller:8776/v3/%\(project_id\)s
openstack endpoint create --region RegionOne \
  volumev3 internal http://controller:8776/v3/%\(project_id\)s
openstack endpoint create --region RegionOne \
  volumev3 admin http://controller:8776/v3/%\(project_id\)s
sudo yum -y install openstack-cinder
cat << EOF | sudo crudini --merge /etc/cinder/cinder.conf
[database]
# ...
connection = mysql+pymysql://cinder:CINDER_DBPASS@controller/cinder
[DEFAULT]
# ...
transport_url = rabbit://openstack:RABBIT_PASS@controller
[DEFAULT]
# ...
auth_strategy = keystone

[keystone_authtoken]
# ...
www_authenticate_uri = http://controller:5000
auth_url = http://controller:5000
memcached_servers = controller:11211
auth_type = password
project_domain_id = default
user_domain_id = default
project_name = service
username = cinder
password = CINDER_PASS
[DEFAULT]
# ...
my_ip = $IP
[oslo_concurrency]
# ...
lock_path = /var/lib/cinder/tmp
EOF
sudo su -s /bin/sh -c "cinder-manage db sync" cinder
cat << EOF | sudo crudini --merge /etc/nova/nova.conf
[cinder]
os_region_name = RegionOne
EOF
sudo systemctl restart openstack-nova-api.service
sudo systemctl enable openstack-cinder-api.service openstack-cinder-scheduler.service
sudo systemctl start openstack-cinder-api.service openstack-cinder-scheduler.service
}

install_compute() {
sudo yum -y install lvm2 device-mapper-persistent-data
sudo systemctl enable lvm2-lvmetad.service
sudo systemctl start lvm2-lvmetad.service
sudo pvcreate $CINDER_DEV
sudo vgcreate cinder-volumes $CINDER_DEV
sudo yum -y install openstack-cinder targetcli python-keystone
cat << EOF | sudo crudini --merge /etc/cinder/cinder.conf
[database]
# ...
connection = mysql+pymysql://cinder:CINDER_DBPASS@controller/cinder
[DEFAULT]
# ...
transport_url = rabbit://openstack:RABBIT_PASS@controller
[DEFAULT]
# ...
auth_strategy = keystone

[keystone_authtoken]
# ...
www_authenticate_uri = http://controller:5000
auth_url = http://controller:5000
memcached_servers = controller:11211
auth_type = password
project_domain_id = default
user_domain_id = default
project_name = service
username = cinder
password = CINDER_PASS
[DEFAULT]
# ...
my_ip = $IP
[lvm]
volume_driver = cinder.volume.drivers.lvm.LVMVolumeDriver
volume_group = cinder-volumes
iscsi_protocol = iscsi
iscsi_helper = lioadm
[DEFAULT]
# ...
enabled_backends = lvm
[DEFAULT]
# ...
glance_api_servers = http://controller:9292
[oslo_concurrency]
# ...
lock_path = /var/lib/cinder/tmp
EOF
sudo systemctl enable openstack-cinder-volume.service target.service
sudo systemctl start openstack-cinder-volume.service target.service
}

if [ $# -ne 1 ]; then
  exit
fi

if [ $1 = "controller" ]; then
  install_controller
elif [ $1 = "compute" ]; then
  install_compute
fi
